package weissmoon.electromagictools.lib;

/**
 * Created by Weissmoon on 9/3/19.
 */
public class Strings {

    public static final class Items{
        public static final String SCRIBING_TOOLS_NAME = "itemElectricScribingTools";
        public static final String ELECTRIC_GOGGLES_NAME = "itemElectricGoggles";
        public static final String ELECTRIC_BOOTS_NAME = "itemElectricBoots";
        public static final String NANO_GOGGLES_NAME = "itemNanoGoggles";
        public static final String NANO_BOOTS_NAME = "itemNanoBoots";
        public static final String QUANTUM_GOGGLES_NAME = "itemQuantumGoggles";
        public static final String QUANTUM_BOOTS_NAME = "itemQuantumBoots";
        public static final String SOLAR_GOGGLES_NAME = "itemSolarGoggles";
        public static final String ELECTRIC_HOE_NAME = "itemElectricHoe";
        public static final String THAUMIUM_DRILL_NAME = "itemThaumiumDrill";
        public static final String ROCKBREAKER_DRILL_NAME = "itemRockbreakerDrill";
        public static final String DIAMOND_CHAINSAW_NAME = "itemDiamondChainsaw";
        public static final String THAUMIUM_CHAINSAW_NAME = "itemThaumiumChainsaw";
        public static final String STREAM_CHAINSAW_NAME = "itemStreamChainsaw";
        public static final String IRON_OMNITOOL_NAME = "itemIronOmniTool";
        public static final String DIAMOND_OMNITOOL_NAME = "itemDiamondOmniTool";
        public static final String THAUMIUM_OMNITOOL_NAME = "itemThaumiumOmniTool";
        public static final String STORMBRINGER_NAME = "itemStormCaster";
        public static final String MJÖLNIR_NAME = "itemMjolnir";
        public static final String STORMBREAKER_NAME = "itemStormBreaker";

        public static final String MATERIALS_NAME = "itemMaterial";
        public static final String[] Materials = {
                "UraniumCluster",       //0
                "LightningSummoner",    //1
                "FeatherMesh",          //2
                "Glue",                 //3
                "DuctTape",             //4
                "RubberBall",           //5
                "CardBoard",            //6
                "FeatherWing",          //7
                "TaintedFeather",       //8
                "ThaumiumWing",         //9
                "UUMatterDrop",         //10
                "CrushedAmberOre",      //11
                "PurifiedAmberOre",     //12
                "CrushedCinnabarOre",   //13
                "PurifiedCinnabarOre"   //14
        };
    }
}
